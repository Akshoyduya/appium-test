# Using Appium to Automate UI Testing of Android and iOS Apps with Java

## Overview

### What is Appium? 

Appium is an open source test automation framework for use with native, hybrid and mobile web apps. 
It drives iOS, Android, and Windows apps using the WebDriver protocol. Appium is built on the idea that testing native apps shouldn't require including an SDK or recompiling your app. And that you should be able to use your preferred test practices, frameworks, and tools. Appium also has made design and tool decisions to encourage a vibrant contributing community.

### Why Appium? 

As a test developer it is quite hard to create a good automated test suite that works on the iOS and Android platforms. Both Android and iOS have a couple of different test frameworks that work well but have nothing to do with each other. So you then have to create and maintain two separate test suites, one for Android and one for iOS. They are also written in different languages and with different development tools (IDEs) so even if the applications work the same it is hard to share any code between the tests for the different platforms.
But this is where Appium comes in. From Appium's own website:

Appium is “cross-platform”: it allows you to write tests against multiple platforms (iOS, Android), using the same API. This enables code reuse between iOS and Android testsuites.

## System Requirements

**iOS**

- Mac OSX
- XCode w/ Command Line Tools

**Android**

- Mac OSX or Windows or Linux
- Android SDK ≥ 16

## Running the tests

To run your tests simply you need to  execute the following command from inside the project directory. 

```
mvn test
```